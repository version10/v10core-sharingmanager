# SharingManager #
![Release](https://img.shields.io/badge/release-v3.3.2-green.svg)

The aim of this project is to provide a easy lightweight module to share dynamic contents with Facebook and Twitter.

## Features

* easy to use
* super lightweight
* Open Facebook sharing with Javascript
* Open Twitter sharing with Javascript
* Add Sharing event with attributes
* Auto remove `s` query string parameter
* Facebook Base64 encoding custom parameter

## Sharing type

* Facebook
* Messenger
* Twitter
* Linkedin
* Pinterest
* WhatsApp
* Mailto
* Print
* Copy

# Dependencies

* jQuery
* [Facebook SDK (App ID)](https://developers.facebook.com/docs/javascript/quickstart)
* Twitter Widget

### Facebook script
```
    <script>
      window.fbAsyncInit = function() {
        FB.init({
          appId            : 'your-app-id',
          autoLogAppEvents : true,
          xfbml            : true,
          version          : 'v3.2'
        });
      };
    </script>
    <script async defer src="https://connect.facebook.net/en_US/sdk.js"></script>
```

### Twitter widget
```
    <script>window.twttr = (function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0],
        t = window.twttr || {};
      if (d.getElementById(id)) return t;
      js = d.createElement(s);
      js.id = id;
      js.src = "https://platform.twitter.com/widgets.js";
      fjs.parentNode.insertBefore(js, fjs);

      t._e = [];
      t.ready = function(f) {
        t._e.push(f);
      };

      return t;
    }(document, "script", "twitter-wjs"));</script>
```

# Installing

* include in `/web/package.json`

## Using

```javascript
    var sm = new V10Core.SharingManager({OPTIONS});
```

## Options

Option | Type | Default | Description
------------ | ------ | --------- | -------------
sharingAttribute | String | `'data-sharing-manager'` | attribute of all sharing button
sharingAttributeParameters | String | `'data-sharing-manager-params'` | attribute parameters of all sharing button
hashtags | String | `''` | Twitter hashtags
via | String | `''` | Twitter via

## Initialisation of SharingManager

```javascript
    jQuery(document).ready(function($) {
        var sm = new V10Core.SharingManager({
            hashtags:'YOUR_HASHTAGS',
            via:'MY_VIA',
            facebookAppId:''
        });
    });
```

## Sharing with Attributes

#### Parameters Facebook

Parameter | Type | Default | Description
------------ | ------ | --------- | -------------
href | String | `''` | Link url
title | String | `og:title` | Name or title
image | String | `og:image` | Picture
description | String | `og:description` | Description

```html
    <a href='#'
        data-sharing-manager='facebook'
        data-sharing-manager-params='{
            "href": "http://www.your-web-site.com?contentID=1",
            "title":"MY CONTENT TITLE"
         }'>
        Share
    </a>
```

#### Parameters Messenger

Parameter | Type | Default | Description
------------ | ------ | --------- | -------------
href | String | `''` | Link url

```html
    <a href='#'
        data-sharing-manager='messenger'
        data-sharing-manager-params='{
            "href": "http://www.your-web-site.com?contentID=1"
         }'>
        Share
    </a>
```

#### Parameters Twitter

Parameter | Type | Default | Description
------------ | ------ | --------- | -------------
text | String | `''` | Text of the message
url | String | `''` | Link url

```html
    <a href='#'
        data-sharing-manager='twitter'
        data-sharing-manager-params='{
            "text":"MY CONTENT TITLE",
            "url": "http://www.your-web-site.com?contentID=1"
         }'>
        Share
    </a>
```

#### Parameters Linkedin

Parameter | Type | Default | Description
------------ | ------ | --------- | -------------
url | String | `''` | Link url

```html
    <a href='#'
        data-sharing-manager='linkedin'
        data-sharing-manager-params='{
            "url": "http://www.your-web-site.com?contentID=1"
         }'>
        Share
    </a>
```

#### Parameters Pinterest

Parameter | Type | Default | Description
------------ | ------ | --------- | -------------
url | String | `''` | Link url
title | String | `''` | Name or title
media | String | `''` | Picture
description | String | `''` | Description

```html
    <a href='#'
        data-sharing-manager='pinterest'
        data-sharing-manager-params='{
            "url": "http://www.your-web-site.com?contentID=1"
         }'>
        Share
    </a>
```

#### Parameters WhatsApp

Parameter | Type | Default | Description
------------ | ------ | --------- | -------------
text | String | `''` | Text
url | String | `''` | Link url

```html
    <a href='#'
        data-sharing-manager='whatsapp'
        data-sharing-manager-params='{
            "text": "your text"
            "url": "http://www.your-web-site.com?contentID=1"
         }'>
        Share
    </a>
```

#### Parameters Mailto

Parameter | Type | Default | Description
------------ | ------ | --------- | -------------
subject | String | `''` | Subject
body | String | `''` | Content
title | String | `''` | Title

```html
    <a href='#'
        data-sharing-manager='mailto'
        data-sharing-manager-params='{
            "subject": "my subject",
            "body": "my body",
            "title": "my title"
         }'>
        Share
    </a>
```

#### Parameters Print

```html
    <a href='#'
        data-sharing-manager='print'
        data-sharing-manager-params='{}'>
        Share
    </a>
```

#### Parameters Copy

Parameter | Type | Default | Description
------------ | ------ | --------- | -------------
url | String | `''` | Link url
feedbackClass | String | `'copy'` | Feedback class
feedbackTime | Int | `2.5` | Feedback duration before hide

```html
    <a href='#'
        data-sharing-manager='copy'
        data-sharing-manager-params='{
            "url": "http://www.your-web-site.com?contentID=1",
            "feedbackClass": "copy",
            "feedbackTime": "2.5",
         }'>
        Copy Share Link
    </a>
```

## Methods

*addSharingTo*

You can Add sharing on all `sharingAttribute` inner a container.

```
    sm.addSharingTo($('YOUR_CONTAINER'));
```

*addFacebookSharingTo*

You can Add Facebook sharing on HTML Element

```
    sm.addFacebookSharingTo($(elem), {Parameters_Facebook});
```

*addMessengerSharingTo*

You can Add Messenger sharing on HTML Element

```
    sm.addMessengerSharingTo($(elem), {Parameters_Facebook});
```

*addTwitterSharingTo*

You can Add Twitter sharing on HTML Element

```
    sm.addTwitterSharingTo($(elem), {Parameters_Twitter});
```

*addLinkedinSharingTo*

You can Add Linkedin sharing on HTML Element

```
    sm.addLinkedinSharingTo($(elem), {Parameters_Linkedin});
```

*addPinterestSharingTo*

You can Add Pinterest sharing on HTML Element

```
    sm.addPinterestSharingTo($(elem), {Parameters_Pinterest});
```

*addMailtoSharingTo*

You can Add Mailto sharing on HTML Element

```
    sm.addMailtoSharingTo($(elem), {Parameters_Facebook});
```

*addPrintSharingTo*

You can Add Print sharing on HTML Element

```
    sm.addPrintSharingTo($(elem), {Parameters_Print});
```


### Credits ###

- Michaël Chartrand (SirKnightDragoon)
