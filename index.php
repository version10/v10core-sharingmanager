<!DOCTYPE html>
<html lang="fr" dir="ltr">
<head>
    <meta charset="utf-8"/>
    <meta name="robots" content="index, follow"/>

    <title>Sharing manager</title>

    <meta name="viewport" content="width=device-width, maximum-scale=1.0, minimum-scale=1.0, initial-scale=1, user-scalable=no"/>

    <meta property="og:title" content="Sharing manager"/>
    <meta property="og:description" content="description..."/>
    <meta property="og:image" content=""/>

    <style>

    </style>
</head>

<body class="screen-medium">

        <h1>Facebook</h1>

        <a href='#'
           data-sharing-manager='facebook'
           data-sharing-manager-params='{
            "href": "https://version10.ca"
         }'>
            Share Facebook
        </a>

        <h1>Messenger</h1>

        <a href='#'
           data-sharing-manager='messenger'
           data-sharing-manager-params='{
            "href": "https://version10.ca"
         }'>
            Share Messenger
        </a>

        <h1>Twitter</h1>

        <a href='#'
           data-sharing-manager='twitter'
           data-sharing-manager-params='{
            "url": "https://version10.ca"
         }'>
            Share Twitter
        </a>

        <h1>Linkedin</h1>

        <a href='#'
           data-sharing-manager='linkedin'
           data-sharing-manager-params='{
            "url": "https://version10.ca"
         }'>
            Share Linkedin
        </a>

        <h1>WhatsApp</h1>

        <a href='#'
           data-sharing-manager='whatsapp'
           data-sharing-manager-params='{
            "text": "your text"
            "url": "https://version10.ca"
         }'>
            Share
        </a>

        <h1>Pinterest</h1>

        <a href='#'
           data-sharing-manager='pinterest'
           data-sharing-manager-params='{
            "url": "https://version10.ca"
         }'>
            Share Pinterest
        </a>

        <a href='#'
           data-sharing-manager='mailto'
           data-sharing-manager-params='{
            "href": "https://version10.ca"
         }'>
            Share Mailto
        </a>

        <h1>Mailto</h1>

        <a href='#'
           data-sharing-manager='mailto'
           data-sharing-manager-params='{
            "subject": "my subject",
            "body": "my body",
            "title": "my title"
         }'>
            Share Mailto
        </a>

        <h1>Print</h1>

        <a href='#'
           data-sharing-manager='print'
           data-sharing-manager-params='{}'>
            Share Print
        </a>

        <h1>Copy</h1>

        <a href='#'
           data-sharing-manager='copy'
           data-sharing-manager-params='{
            "url": "http://www.your-web-site.com?contentID=1"
         }'>
            Copy Share Link
        </a>

        <script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
        <script type="text/javascript" src="dist/SharingManager.min.js"></script>

        <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>


        <script>
            jQuery(document).ready(function($) {
                var sm = new V10Core.SharingManager({
                    hashtags:'YOUR_HASHTAGS',
                    via: 'MY_VIA',
                    facebookAppId:'your-app-id'
                });
            });
        </script>

        <script>
            window.fbAsyncInit = function() {
                FB.init({
                    appId            : 'your-app-id',
                    autoLogAppEvents : true,
                    xfbml            : true,
                    version          : 'v3.2'
                });
            };
        </script>
        <script async defer src="https://connect.facebook.net/en_US/sdk.js"></script>

        <script>window.twttr = (function(d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0],
                    t = window.twttr || {};
                if (d.getElementById(id)) return t;
                js = d.createElement(s);
                js.id = id;
                js.src = "https://platform.twitter.com/widgets.js";
                fjs.parentNode.insertBefore(js, fjs);

                t._e = [];
                t.ready = function(f) {
                    t._e.push(f);
                };

                return t;
            }(document, "script", "twitter-wjs"));</script>
</body>

</html>
