//SharingManager v3.3.2
if(typeof V10Core === "undefined") V10Core = {};

V10Core.SharingManager = function(options){
    this.options = {
    	sharingAttribute:'data-sharing-manager',
        sharingAttributeParameters:'data-sharing-manager-params',
        facebookAppId:'',
        hashtags: '',
        via:''
    };

    for (o in options) {
        this.options[o] = options[o];
    }

    this.addSharingTo($("body"));
    window.history.replaceState({}, window.document.title, this.removeURLParameter(window.location.href, 's'));
}

V10Core.SharingManager.prototype.addSharingTo = function(container){
    var _this = this;

    $("["+this.options.sharingAttribute+"='facebook']", container).each(function(){
        try{
            _this.addFacebookSharingTo($(this), JSON.parse($(this).attr(_this.options.sharingAttributeParameters)));
        }catch(error){
            console.error("Error to parse ", $(this).attr(_this.options.sharingAttributeParameters));
        }
    });

    $("["+this.options.sharingAttribute+"='messenger']", container).each(function(){
        try{
            _this.addMessengerSharingTo($(this), JSON.parse($(this).attr(_this.options.sharingAttributeParameters)));
        }catch(error){
            console.error("Error to parse ", $(this).attr(_this.options.sharingAttributeParameters));
        }
    });

    $("["+this.options.sharingAttribute+"='twitter']", container).each(function(){
        try{
            _this.addTwitterSharingTo($(this), JSON.parse($(this).attr(_this.options.sharingAttributeParameters)));
        }catch(error){
            console.error("Error to parse ", $(this).attr(_this.options.sharingAttributeParameters));
        }
    });

    $("["+this.options.sharingAttribute+"='linkedin']", container).each(function(){
        try{
            _this.addLinkedinSharingTo($(this), JSON.parse($(this).attr(_this.options.sharingAttributeParameters)));
        }catch(error){
            console.error("Error to parse ", $(this).attr(_this.options.sharingAttributeParameters));
        }
    });

    $("["+this.options.sharingAttribute+"='pinterest']", container).each(function(){
        try{
            _this.addPinterestSharingTo($(this), JSON.parse($(this).attr(_this.options.sharingAttributeParameters)));
        }catch(error){
            console.error("Error to parse ", $(this).attr(_this.options.sharingAttributeParameters));
        }
    });

    $("["+this.options.sharingAttribute+"='whatsapp']", container).each(function(){
        try{
            _this.addWhatsAppSharingTo($(this), JSON.parse($(this).attr(_this.options.sharingAttributeParameters)));
        }catch(error){
            console.error("Error to parse ", $(this).attr(_this.options.sharingAttributeParameters));
        }
    });

    $("["+this.options.sharingAttribute+"='mailto']", container).each(function(){
        try{
            _this.addMailtoSharingTo($(this), JSON.parse($(this).attr(_this.options.sharingAttributeParameters)));
        }catch(error){
            console.error("Error to parse ", $(this).attr(_this.options.sharingAttributeParameters));
        }
    });

    $("["+this.options.sharingAttribute+"='print']", container).each(function(){
        try{
            _this.addPrintSharingTo($(this), JSON.parse($(this).attr(_this.options.sharingAttributeParameters)));
        }catch(error){
            console.error("Error to parse ", $(this).attr(_this.options.sharingAttributeParameters));
        }
    });

    $("["+this.options.sharingAttribute+"='copy']", container).each(function(){
        try{
            _this.addCopySharingTo($(this), JSON.parse($(this).attr(_this.options.sharingAttributeParameters)));
        }catch(error){
            console.error("Error to parse ", $(this).attr(_this.options.sharingAttributeParameters));
        }
    });
}

V10Core.SharingManager.prototype.addFacebookSharingTo = function(elem, parameters){
    var params = {
        href: ''
    };

    for (o in parameters) {
        params[o] = parameters[o];
    }

    var query = this.utf8_to_b64(JSON.stringify(parameters));

    var url = params.href;

    if(params.href.indexOf('?') == -1){
        url += "?s=" + query;
    }else{
        url += "&s=" + query;
    }

    elem.off("click");
	elem.on("click", function(e){
		e.preventDefault();
		FB.ui({
			method: 'share',
            href:url,
            mobile_iframe: true
		});
	});
}

V10Core.SharingManager.prototype.addMessengerSharingTo = function(elem, parameters){
    var _this = this;

    var params = {
        href: ''
    };

    for (o in parameters) {
        params[o] = parameters[o];
    }

    elem.off("click");
    elem.on("click", function(e){
        e.preventDefault();
        window.open('fb-messenger://share?link=' + encodeURIComponent(params.href) + '&app_id=' + encodeURIComponent(_this.options.facebookAppId));
    });
}

V10Core.SharingManager.prototype.addTwitterSharingTo = function(elem, parameters){
    var params = {
        text:'',
        url: ''
    };

    for (o in parameters) {
        params[o] = parameters[o];
    }

    var via = this.options.via == '' ? '' : "&via="+this.options.via;

	elem.attr('href', "https://twitter.com/intent/tweet?text="+encodeURIComponent(params.text)+"&url="+encodeURIComponent(params.url)+"&hashtags="+this.options.hashtags+via);
}

V10Core.SharingManager.prototype.addLinkedinSharingTo = function(elem, parameters){
    var params = {
        url: ''
    };

    for (o in parameters) {
        params[o] = parameters[o];
    }

    elem.attr('href', "https://www.linkedin.com/shareArticle?mini=true&url="+encodeURIComponent(params.url));

    elem.on("click", function(e){
        e.preventDefault();

        var width = 500;
        var height = 500;
        var left = parseInt((screen.availWidth/2) - (width/2) + window.screenX);
        var top = parseInt((screen.availHeight/2) - (height/2) + window.screenY);

        var windowFeatures = "width=" + width + ",height=" + height + ",status=no,resizable=no,scrollbars=no,left=" + left + ",top=" + top + "screenX=" + left + ",screenY=" + top;

        window.open(this.href,'Linkedin', windowFeatures);
    });
}

V10Core.SharingManager.prototype.addWhatsAppSharingTo = function(elem, parameters){
    var params = {
        text: '',
        url: ''
    };

    for (o in parameters) {
        params[o] = parameters[o];
    }

    var href = "https://wa.me/?text=";

    if(params.text != ''){
        href += encodeURIComponent(params.text);

        if(params.url != ''){
            href += encodeURIComponent(" " + params.url);
        }
    }else{
        if(params.url != ''){
            href += encodeURIComponent(params.url);
        }
    }

    elem.attr('href', href);

    elem.on("click", function(e){
        e.preventDefault();

        var width = 500;
        var height = 500;
        var left = parseInt((screen.availWidth/2) - (width/2) + window.screenX);
        var top = parseInt((screen.availHeight/2) - (height/2) + window.screenY);

        var windowFeatures = "width=" + width + ",height=" + height + ",status=no,resizable=no,scrollbars=no,left=" + left + ",top=" + top + "screenX=" + left + ",screenY=" + top;

        window.open(this.href,'WhatsApp', windowFeatures);
    });
}

V10Core.SharingManager.prototype.addPinterestSharingTo = function(elem, parameters){
    var params = {
        url: '',
        media:'',
        title:'',
        description:''
    };

    for (o in parameters) {
        params[o] = parameters[o];
    }

    var href = "http://pinterest.com/pin/create/link/?url="+encodeURIComponent(params.url);

    if(params.media != ''){
        href += "&media="+encodeURIComponent(params.media);
    }

    if(params.title != ''){
        href += "&title="+encodeURIComponent(params.title);
    }

    if(params.description != ''){
        href += "&description="+encodeURIComponent(params.description);
    }

    elem.attr('href', href);

    elem.on("click", function(e){
        e.preventDefault();

        var width = 800;
        var height = 500;
        var left = parseInt((screen.availWidth/2) - (width/2) + window.screenX);
        var top = parseInt((screen.availHeight/2) - (height/2) + window.screenY);

        var windowFeatures = "width=" + width + ",height=" + height + ",status=no,left=" + left + ",top=" + top + "screenX=" + left + ",screenY=" + top;

        window.open(this.href,'Pinterest', windowFeatures);
    });
}

V10Core.SharingManager.prototype.addMailtoSharingTo = function(elem, parameters){
    var params = {};

    for (o in parameters) {
        params[o] = parameters[o];
    }

    var href = "mailto:?";

    if(params.subject != ''){
        href += "subject="+params.subject+"&";
    }

    if(params.body != ''){
        href += "body="+params.body+"&";
    }

    if(params.title != ''){
        href += "title="+params.title+"&";
    }

    elem.attr('href', href);
}

V10Core.SharingManager.prototype.addPrintSharingTo = function(elem, parameters){
    var params = {};

    for (o in parameters) {
        params[o] = parameters[o];
    }

    elem.on("click", function(e){
        e.preventDefault();
        window.print();
    });
}

V10Core.SharingManager.prototype.addCopySharingTo = function(elem, parameters){
    var params = {
        url: '',
        feedbackClass: 'copy',
        feedbackTime: 2.5,
    };

    for (o in parameters) {
        params[o] = parameters[o];
    }

    elem.on("click", function(e){
        e.preventDefault();

        var el = document.createElement('textarea');
        el.value = params.url;
        document.body.appendChild(el);
        el.select();
        document.execCommand('copy');
        document.body.removeChild(el);

        elem.addClass(params.feedbackClass);

        setTimeout(function(){
            elem.removeClass(params.feedbackClass);
        }, params.feedbackTime * 1000);
    });
}

V10Core.SharingManager.prototype.utf8_to_b64 = function( str ) {
    return window.btoa(unescape(encodeURIComponent( str )));
}

V10Core.SharingManager.prototype.b64_to_utf8 = function( str ) {
    return decodeURIComponent(escape(window.atob( str )));
}

V10Core.SharingManager.prototype.removeURLParameter = function(url, parameter) {
    //prefer to use l.search if you have a location/link object
    var urlparts = url.split('?');
    if (urlparts.length >= 2) {

        var prefix = encodeURIComponent(parameter) + '=';
        var pars = urlparts[1].split(/[&;]/g);

        //reverse iteration as may be destructive
        for (var i = pars.length; i-- > 0;) {
            //idiom for string.startsWith
            if (pars[i].lastIndexOf(prefix, 0) !== -1) {
                pars.splice(i, 1);
            }
        }

        url = urlparts[0];
        if(pars.join('&') != ''){
            url +='?'+pars.join('&');
        }

        return url;
    } else {
        return url;
    }
}

V10Core.SharingManager.prototype.version = "3.3.2";
